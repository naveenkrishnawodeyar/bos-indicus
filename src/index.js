import React from 'react';
import ReactDOM from 'react-dom/client';
import App from './App'
import '../node_modules/bootstrap/dist/css/bootstrap.css';

// const root = ReactDOM.createRoot(document.getElementById('root'));
// root.render(
//   <React.StrictMode>
//     <App />
//   </React.StrictMode>
// );

let container = document.getElementById('root');
let root = ReactDOM.createRoot(container);
root.render(<App/>);

